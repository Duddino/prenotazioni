use serde::{Deserialize, Serialize};

#[derive(Serialize)]
pub struct LoginResponse {
    pub ok: bool,
    pub is_admin: bool,
}

impl LoginResponse {
    pub fn new(ok: bool, is_admin: bool) -> Self {
        LoginResponse { ok, is_admin }
    }
}

#[derive(Deserialize)]
pub struct LoginInfo {
    pub username: String,
    pub password: String,
}
