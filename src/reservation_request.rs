use serde::Deserialize;

#[derive(Deserialize)]
pub struct ReservationRequest {
    // Ignore every reservation made before min_h (usually 8)
    pub min_h: u32,
    // Ignore every reservation made after max_h (usually 13)
    pub max_h: u32,
    pub min_day: u32,
    pub max_day: u32,
    pub laboratory: u32,
}
