table! {
    reservations (id) {
        day -> Integer,
        hour -> Integer,
        reserved_to -> Integer,
        laboratory -> Integer,
        id -> Unsigned<Bigint>,
        description -> Text,
    }
}

table! {
    users (id) {
        username -> Varchar,
        password -> Varchar,
        id -> Unsigned<Bigint>,
        is_admin -> Bool,
    }
}

allow_tables_to_appear_in_same_query!(
    reservations,
    users,
);
