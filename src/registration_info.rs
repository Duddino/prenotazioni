use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Registration {
    pub name: String,
    pub password: String,
}
