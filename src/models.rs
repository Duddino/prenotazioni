use super::schema::{reservations, users};
use diesel::Queryable;
use serde::{Deserialize, Serialize};

#[derive(Queryable)]
pub struct User {
    pub username: String,
    pub password: String,
    pub id: u64,
    pub is_admin: bool,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub password: String,
    pub is_admin: bool,
}

#[derive(Queryable, Serialize, Clone, Debug)]
pub struct Reservation {
    pub day: i32,
    pub hour: i32,
    pub reserved_to: i32,
    pub laboratory: i32,
    pub id: u64,
    pub description: String,
}

#[derive(Insertable)]
#[table_name = "reservations"]
pub struct NewReservation {
    pub day: i32,
    pub hour: i32,
    pub reserved_to: i32,
    pub laboratory: i32,
    pub description: String,
}

impl NewReservation {
    pub fn new(day: i32, hour: i32, reserved_to: i32, laboratory: i32, description: String) -> Self {
        Self {
            day,
            hour,
            reserved_to,
            laboratory,
            description,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ReservationString {
    pub day: i32,
    pub hour: i32,
    pub reserved_to: String,
    pub laboratory: i32,
    pub description: String,
}

impl ReservationString {
    pub fn from_reservation(r: Reservation, name: String) -> Self {
        Self {
            day: r.day,
            hour: r.hour,
            reserved_to: name,
            laboratory: r.laboratory,
            description: r.description,
        }
    }
}
