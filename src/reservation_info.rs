use serde::Deserialize;

#[derive(Deserialize)]
pub struct ReservationInfo {
    pub day: u32,
    pub hour: u32,
    pub laboratory: u32,
    pub description: String,
}

#[derive(Deserialize)]
pub struct ReservationNoDescription {
    pub day: u32,
    pub hour: u32,
    pub laboratory: u32,
}
