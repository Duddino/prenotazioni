use crate::models::*;
use crate::registration_info::Registration;
use diesel::mysql::MysqlConnection;
use diesel::prelude::*;
use std::env;
use std::time::{SystemTime, UNIX_EPOCH};

pub fn establish_connection() -> MysqlConnection {
    println!("Connecting to mysql...");

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    MysqlConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn login_user(m_username: &str, m_password: &str, connection: &MysqlConnection) -> Option<User> {
    use crate::schema::users::dsl::*;
    let user: Option<User> = users
        .filter(username.eq(m_username))
        .first(connection)
        .optional()
        .ok()
        .flatten();
    if let Some(user) = user {
        if bcrypt::verify(m_password, &user.password).unwrap() {
            Some(user)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn get_user_id_from_name(m_username: &str, connection: &MysqlConnection) -> Option<u64> {
    use crate::schema::users::dsl::*;
    users
        .filter(username.eq(m_username))
        .first::<User>(connection)
        .optional()
        .ok()
        .flatten()
        .map(|u| u.id)
}

pub fn reserve(
    m_day: i32,
    m_hour: i32,
    user_id: u64,
    m_laboratory: i32,
    m_description: String,
    connection: &MysqlConnection,
    bypass_check: bool,
) -> bool {
    use crate::schema::reservations::{self, dsl::*};
    if !bypass_check && !is_a_valid_reservation(m_day, m_hour, is_admin(user_id, connection)) {
        return false;
    }
    // Theoretically one could reserve a hour twice! (or thrice!)
    let reservation: Option<Reservation> = reservations
        .filter(day.eq(m_day))
        .filter(hour.eq(m_hour).or(hour.eq(m_hour - 1)).or(hour.eq(m_hour + 1)))
        .filter(laboratory.eq(m_laboratory))
        .filter(reserved_to.ne(user_id as i32))
        .first(connection)
        .optional()
        .ok()
        .flatten();
    if bypass_check || reservation.is_none() {
        diesel::insert_into(reservations::table)
            .values(NewReservation::new(m_day, m_hour, user_id as i32, m_laboratory, m_description))
            .execute(connection)
            .unwrap();
        true
    } else {
        debug_assert!(false, "This shouldn't happen!");
        false
    }
}

pub fn is_admin(user_id: u64, connection: &MysqlConnection) -> bool {
    use crate::schema::users::dsl::*;
    users
        .filter(id.eq(user_id))
        .first::<User>(connection)
        .ok()
        .expect("Invalid user id")
        .is_admin
}

fn is_a_valid_reservation(m_day: i32, m_hour: i32, is_admin: bool) -> bool {
    let not_available_days = 2;
    let current_day = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time cannot go backwards")
        .as_millis()
        / (1000 * 60 * 60 * 24);
    if is_admin {
        true
    } else {
        current_day + not_available_days <= (m_day as u128)
            && m_hour <= 12
            && m_hour >= 8
            && current_day + 14 > (m_day as u128)
            && m_day % 7 != 3
    }
}

pub fn get_user_name_by_id(m_id: u64, connection: &MysqlConnection) -> Option<String> {
    use crate::schema::users::dsl::*;
    users
        .filter(id.eq(m_id))
        .first::<User>(connection)
        .optional()
        .ok()
        .flatten()
        .map(|u| u.username)
}

pub fn get_reservations(
    min_h: i32,
    max_h: i32,
    min_day: i32,
    max_day: i32,
    m_laboratory: i32,
    connection: &MysqlConnection,
) -> Vec<ReservationString> {
    use crate::schema::reservations::dsl::*;
    reservations
        .filter(hour.ge(min_h))
        .filter(hour.le(max_h))
        .filter(day.ge(min_day))
        .filter(day.le(max_day))
        .filter(laboratory.eq(m_laboratory))
        .load::<Reservation>(connection)
        .unwrap()
        .into_iter()
        .map(|r| {
            ReservationString::from_reservation(
                r.clone(),
                get_user_name_by_id(r.reserved_to as u64, connection)
                    .unwrap_or("Unexpected error".to_owned()),
            )
        })
        .collect()
}

pub fn cancel(
    m_day: i32,
    m_hour: i32,
    user_id: u64,
    m_laboratory: i32,
    connection: &MysqlConnection,
) -> bool {
    use crate::schema::reservations::dsl::*;
    let reservation: Option<Reservation> = reservations
        .filter(day.eq(m_day))
        .filter(hour.eq(m_hour))
        .filter(laboratory.eq(m_laboratory))
        .first(connection)
        .optional()
        .ok()
        .flatten();
    if let Some(reservation) = reservation {
        if reservation.reserved_to as u64 == user_id || is_admin(user_id, connection) {
            diesel::delete(reservations.filter(id.eq(reservation.id)))
                .execute(connection)
                .is_ok()
        } else {
            false
        }
    } else {
        false
    }
}

pub fn reserve_admin(
    m_day: i32,
    m_hour: i32,
    user_id: u64,
    m_laboratory: i32,
    m_reserved_to: &str,
    connection: &MysqlConnection,
) -> bool {
    if is_admin(user_id, connection) {
        let reserved_to_id = get_user_id_from_name(&m_reserved_to, connection);
        if let Some(reserved_to_id) = reserved_to_id {
            cancel(m_day, m_hour, user_id, m_laboratory, connection);
            reserve(
                m_day,
                m_hour,
                reserved_to_id,
                m_laboratory,
                "".to_owned(),
                connection,
                true,
            )
        } else {
            false
        }
    } else {
        false
    }
}

pub fn register(user_id: u64, registration: &Registration, connection: &MysqlConnection) -> bool {
    use crate::schema::users::{self, dsl::*};
    if self::is_admin(user_id, connection) {
        let user: Option<User> = users
            .filter(username.eq(registration.name.clone()))
            .first(connection)
            .optional()
            .ok()
            .flatten();
        if user.is_none() {
            diesel::insert_into(users::table)
                .values(NewUser {
                    username: registration.name.clone(),
                    password: bcrypt::hash(registration.password.clone(), 12).expect("bcrypt failed"),
                    is_admin: false,
                })
                .execute(connection)
                .is_ok()
        } else {
            println!("That user already exists!");
            false
        }
    } else {
        false
    }
}

pub fn change_password(user_id: u64, registration: &Registration, connection: &MysqlConnection) -> bool {
    use crate::schema::users::dsl::*;
    if self::is_admin(user_id, connection) {
        let hashed_password = bcrypt::hash(registration.password.clone(), 12).expect("bcrypt failed");
        diesel::update(users.filter(username.eq(registration.name.clone()))).set(password.eq(hashed_password)).execute(connection).is_ok()
    } else {
        false
    }
}

pub fn get_users(connection: &MysqlConnection) -> Vec<String> {
    use crate::schema::users::dsl::*;
    if let Ok(result) = users.load::<User>(connection) {
        result.into_iter().map(|u| u.username).collect()
    } else {
        debug_assert!(false, "Users.load failed!!!");
        Vec::new()
    }
}
