#[macro_use]
extern crate diesel;

mod database;
mod login_info;
mod models;
mod registration_info;
mod reservation_info;
mod reservation_request;
mod schema;

use crate::login_info::{LoginInfo, LoginResponse};
use crate::models::ReservationString;
use crate::registration_info::Registration;
use crate::reservation_info::ReservationInfo;
use crate::reservation_request::ReservationRequest;
use actix_session::{CookieSession, Session};
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use diesel::mysql::MysqlConnection;
use dotenv::dotenv;
use reservation_info::ReservationNoDescription;
use serde::Serialize;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let port = std::env::var("PORT").unwrap_or("8080".to_owned());
    println!("Server started on port {}", port);
    HttpServer::new(|| {
        App::new()
            .wrap(CookieSession::signed(&[0; 32]).secure(false))
            .data(database::establish_connection())
            .service(login)
            .service(get_username)
            .service(get_reservations)
            .service(reserve)
            .service(cancel)
            .service(reserve_admin)
            .service(is_admin)
            .service(register)
            .service(get_users)
            .service(change_password)
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}

#[post("/login")]
async fn login(
    login_info: web::Json<LoginInfo>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user = database::login_user(&login_info.username, &login_info.password, &database);
    println!("{} logged in", login_info.username);
    if let Some(user) = user {
        session.set("user_id", user.id).unwrap(); //TODO: Remove unwrap().
        HttpResponse::Ok().json(LoginResponse::new(true, user.is_admin))
    } else {
        HttpResponse::Unauthorized().json(LoginResponse::new(false, false))
    }
}

#[get("/username")]
async fn get_username(session: Session, database: web::Data<MysqlConnection>) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        HttpResponse::Ok().body(
            database::get_user_name_by_id(user_id, &database)
                .unwrap_or("Unexpected error".to_owned()),
        )
    } else {
        HttpResponse::Unauthorized().json("{\"ok\": false, \"reason\": \"You're not logged in\"}")
    }
}

#[post("/prenota")]
async fn reserve(
    reservation_info: web::Json<ReservationInfo>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        let success = database::reserve(
            reservation_info.day as i32,
            reservation_info.hour as i32,
            user_id,
            reservation_info.laboratory as i32,
            reservation_info.description.clone(), //optimize
            &database,
            false,
        );
        if success {
            HttpResponse::Ok().json("{ok: true}")
        } else {
            HttpResponse::Forbidden().json("{ok: false}")
        }
    } else {
        HttpResponse::Unauthorized().json("{\"ok\": false, \"reason\": \"You're not logged in\"}")
    }
}

#[get("/orario")]
async fn get_reservations(
    reservation_request: web::Query<ReservationRequest>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if user_id.is_some() {
        let prenotazioni = database::get_reservations(
            reservation_request.min_h as i32,
            reservation_request.max_h as i32,
            reservation_request.min_day as i32,
            reservation_request.max_day as i32,
            reservation_request.laboratory as i32,
            &database,
        );
        let response = HttpResponse::Ok().json(prenotazioni);
        response
    } else {
        HttpResponse::Unauthorized().json("{\"ok\": false, \"reason\": \"You're not logged in\"}")
    }
}

#[post("/disdici")]
async fn cancel(
    reservation_info: web::Json<ReservationNoDescription>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        let success = database::cancel(
            reservation_info.day as i32,
            reservation_info.hour as i32,
            user_id,
            reservation_info.laboratory as i32,
            &database,
        );
        if success {
            HttpResponse::Ok().json("{ok: true}")
        } else {
            HttpResponse::Forbidden().json("{ok: false}")
        }
    } else {
        HttpResponse::Unauthorized().json("{\"ok\": false, \"reason\": \"You're not logged in\"}")
    }
}

#[post("/admin_prenota")]
async fn reserve_admin(
    reservation: web::Json<ReservationString>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        if database::reserve_admin(
            reservation.day,
            reservation.hour,
            user_id,
            reservation.laboratory,
            &reservation.reserved_to,
            &database,
        ) {
            HttpResponse::Ok().json(Ok { ok: true })
        } else {
            HttpResponse::NotFound().json(Ok { ok: false })
        }
    } else {
        HttpResponse::Unauthorized().json(Ok { ok: false })
    }
}

#[get("/is_admin")]
async fn is_admin(session: Session, database: web::Data<MysqlConnection>) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        if database::is_admin(user_id, &database) {
            HttpResponse::Ok().json("{\"is_admin\": true}")
        } else {
            HttpResponse::Ok().json("{\"is_admin\": false}")
        }
    } else {
        HttpResponse::Unauthorized().json("{\"ok\": false, \"reason\": \"You're not logged in\"}")
    }
}

#[post("/registra")]
async fn register(
    registration: web::Json<Registration>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        let result = database::register(user_id, &registration, &database);
        if result {
            HttpResponse::Ok().json(Ok { ok: true })
        } else {
            HttpResponse::ServiceUnavailable().json(Ok { ok: false })
        }
    } else {
        HttpResponse::Unauthorized().json(Ok { ok: false })
    }
}

#[post("/cambia_password")]
async fn change_password(
    registration: web::Json<Registration>,
    session: Session,
    database: web::Data<MysqlConnection>,
) -> impl Responder {
    let user_id = session.get::<u64>("user_id").ok().flatten();
    if let Some(user_id) = user_id {
        let result = database::change_password(user_id, &registration, &database);
        if result {
            HttpResponse::Ok().json(Ok { ok: true })
        } else {
            HttpResponse::ServiceUnavailable().json(Ok { ok: false })
        }
    } else {
        HttpResponse::Unauthorized().json(Ok { ok: false })
    }

}

#[get("/users")]
async fn get_users(
    database: web::Data<MysqlConnection>,
) -> impl Responder
{
    let users = database::get_users(&database);
    HttpResponse::Ok().json(users)
}

#[derive(Serialize)]
struct Ok {
    pub ok: bool,
}
