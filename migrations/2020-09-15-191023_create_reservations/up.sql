CREATE TABLE reservations (
	id INT PRIMARY KEY,
	day INT NOT NULL,
	hour INT NOT NULL,
	reserved_to INT NOT NULL
)
